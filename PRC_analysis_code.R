#R code to run PRC analyses:
  
#install.packages("PRC", repos = c('https://cajoterbraak.r-universe.dev'))
Test<-TRUE
library(readxl)
library(ggplot2)
library(PRC)

# Input data ----------------------------------------------------
log_abundance_tbl <- readxl::read_xlsx(path = "data/log_abundance.xlsx")
metadata_tbl <- readxl::read_xlsx(path = "data/metadata.xlsx")
all.equal(metadata_tbl$rownames, log_abundance_tbl$rownames)
Design <- metadata_tbl
Design$NDC <- factor(Design$NDC);  Design$Antibiotic <- factor(Design$Antibiotic)
Design$Time.h <- as.numeric(Design$Time.h)
Design$Time.f <- factor(Design$Time.h); Design$Run <- factor(Design$Run) # .F for factor

Timepoints <-sort(unique(Design$Time.h))

Intervals <- diff(Timepoints)

summary(Design)

# the factor levels
Design$NDC <- factor(Design$NDC, levels = rev(c("none","2FL","IMMP87","GOS")))
Design$Antibiotic1 <- factor(Design$Antibiotic, levels = c("none","amoxclav","azith" ))

Design$AB <- with(Design, interaction(NDC,Antibiotic1))
levels(Design$Antibiotic1)
DesignFull <-Design

ids<- DesignFull$Antibiotic1 %in% c("none","amoxclav") #& DesignFull$Time.h > 23
# for azithromycin use c("none","azith")

DesignSub <- DesignFull[ids,]
DesignSub$Antibiotic1 <- factor(DesignSub$Antibiotic1)
levels(DesignSub$Antibiotic1)
summary(DesignFull)
#DesignSub$AB <- DesignSub$Antibiotic1

# rename Antibiotics
Design$Antibiotic<- factor(c("No antibiotic", "Amoxicillin/clavulanate","Azithromycin")[Design$Antibiotic1])
data.frame(Design$Antibiotic, Design$Antibiotic1)

#Design$Block <- factor(Design$Block)
# a complete design!
print(with(Design,table(NDC,Antibiotic, Time.f)))

# extract response variables
Y <- log_abundance_tbl[ids,-1]; rownamesY<-log_abundance_tbl$rownames[ids]

# PRC via vegan -----------------------------------------------------------
library(vegan)
names(Design)
summary(Design)
names(DesignSub)
levels(DesignSub$AB)
myrda <- vegan::rda( Y~Time.f*Antibiotic1*NDC + Condition(Time.f) ,data = DesignSub)
print(myrda)

percExp <-round(100*myrda$CCA$eig[1:5]/sum(myrda$CCA$eig),2)

if (Test){
cnrl <-how(plots = Plots(strata = DesignSub$Treatment_run,type = "free"),
within = Within(type = "none"), nperm = 999)
anova(myrda, by = "axis",permutations=  cnrl, model = "reduced", cutoff = 0.10)
}

#SubtractReferenceValues has been replaced by the function PRC_scores
Design_w_PRCs <- PRC_scores(myrda, focal_factor_name= c("Antibiotic1", "NDC"), referencelevel = c("none","none") , rank = 4, data= DesignSub, flip = TRUE)
print(names(Design_w_PRCs$coefficients))
if (is.list(Design_w_PRCs$coefficients)) print(round(Design_w_PRCs$coefficients[[1]], 2)) else print(round(Design_w_PRCs$coefficients, 2))

names(Design_w_PRCs)
names(Design_w_PRCs$PRCplus)
